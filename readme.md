# Sunday Morning Tech Checklist

This file is best-viewed at 
https://gitlab.com/Rising-Church/technical-documentation/-/blob/master/readme.md

[[_TOC_]]

# Links

Use the Firefox Bookmarks to get to these pages quickly. Hold control and click
to open links if viewing in the text editor.

+ [Obtaining Passwords][obtaining-passwords]
+ [Lyrics Control Panel][local-lyrics-control-panel]
+ [Lyrics View][local-lyrics]
+ [Google Drive folder for recorded audio uploads][audio-uploads]
    + You will need to be logged in as `info@kcrising.church`
+ [Google Drive folder for sermon presentation slides files][presentation-files]
    + You will need to be logged in as `info@kcrising.church`

# 0800: Arrive and Prepare

+ Check thermostat(s)
+ Power on necessary equipment
    + Lights
        + Ceiling lights have dimmer switches on the wall throughout the space
            + Lobby: behind coffee machines
            + Stage: just south of tech stage
            + Congregation seating: just outside south door (by the conference room)
        + Stage spotlights can be turned on with the small, white remote
    + Sound equipment
        + This is usually a single power strip under the tech desk
    + Above-stage TV (for congregation)
        + Must use the small black remote (and you usually have to walk on-stage before it works)
+ Displays/monitors configured
    + Main display works for you
    + Stage display shows lyrics for worship team
    + Above-stage TV shows lyrics/sermon slides for congregation
    + This is usually automatically setup
    + You can switch between worship lyrics and sermon presentation
        + You can set this up with two tabs in a browser, one tab showing the lyrics display, the other with the google slides presentation
            + Ctrl-F5 on the Google Slides presentation to begin presenting, then you can use Ctrl-Tab to switch between the tabs
            + You can pick the correct browser window by mousing over the Firefox icon in the bottom left on the taskbar for a half-second, then clicking the correct window - then you can Ctrl-Tab without any disruption
    + You may also want to setup a Timer Record in Audacity (Menu > Transport > Recording > Timer Record) to ensure you don't forget to record at the appropriate time
        + https://manual.audacityteam.org/man/timer_record.html
        + I usually set up the timer to start recording at 9:55 AM and stop recording at 12:30 PM though I will almost certainly stop it manually before then (by clicking the Stop button)
        + Make sure to select the Ogg format if you setup automatic export
        + You can also have the exported file automatically upload to Google Drive in the correct folder by exporting directly to `/home/rising/GoogleDrive/Recordings`, but please note that this is experimental, so for now, you should go ahead and export to the usual directory (`/home/rising/Documents/recordings`) and just copy it (or upload it the old-fashioned way)
+ Pre-rehearsal prayer
+ Ensure you have all necesary information for setup
    + Song lyrics and order
        + If you happen to have them this early
    + Sermon presentation

# 0830: Worship Rehearsal

+ Verify audio setup
    + Microphones connected appropriately
    + Intruments hooked up to correct channels
    + Auxiliary connections setup correctly (phone pad, violin, etc.)
+ Verify lyrics as the worship team sings them (running through as they play)
+ Start recording (just in case you forget later)

# 0930: Practice Ends

+ Final verification of lyrics with worship leader
+ Final setup tweaks to sound setup
    + Usually mute mic channels while nobody is onstage so kids and whatnot don't send loud sounds through the system
+ Ensure sermon presentation is ready, looks correct, and is functional (with clicker if needed)

# 0945: Early Arrivals

+ Start some nice and chill background music
    + Search for `chill instrumental worship background music` on youtube

# 0950: Final Prep & Check

+ Setup wireless lapel teaching mic
    + Consider swapping in fresh AA batteries if you're not positive they are fresh
        + Generally about once per month
        + Charger for these is right behind the sound board
+ Double-check anything you may not have been able to previously
    + Especially the recording!
+ Set displays to show blank lyrics or welcome slide

# 0955: Prayer

+ Pray!

# 1000: Service Starts with Worship Set 1

+ Unmute any worship team channels that may have been muted

# 1015: Worship Set 1 Ends, Intermission & Announcements Start

+ Triple check that recording is running for sermon
+ Mute non-essential channels to prevent feedback
+ Adjust volumes for quiet announcement-giver
    + Kill reverb on speaker mic if using the same one as worship team
+ Switch display to announcements info (or slides if any, which is unusual)
 
# 1025: Announcements End, Teaching Starts

+ Restore reverb if speaker used a worship member's mic
+ Transition audio setup from announcements to teaching
    + Mute everything except speaker mic
        + Wireless lapel mic is unmarked on channel 5
            + Note the different-looking cable on the input
+ If there's a teaching presentation, switch video display to it (focus the proper windows)
    + For livestream, verify presentation is visible

# 1055: Teaching Ends, Worship Set 2 Starts

+ Transition audio setup back for worship
    + Unmute relevant channels, mute teacher mic
    + Double-check any levels that were changed for teaching and announcements that might interfere with worship's setup
+ If needed, switch video display back to lyrics (focus the proper windows)
    + For livestream, verify lyrics are visible

# 1115: Service End

+ Start outro music after band stops
+ Save, export, and upload the sermon recording
+ Stop livestream after a few minutes
+ Turn off audio equipment, lights, and displays
+ Charge batteries
+ Re-store any extra cables that were used
+ Clean up

# Appendix

## Obtaining Passwords

Sometimes, you may need a certain password to access certain resources to perform these steps. This portion of the document will outline how you can access those passwords.

### From the Rising Computer

Passwords are stored in a password manager at https://bw.lyte.dev under a user with the email tech@kcrising.church as well as additional users for anybody that needs access. The password for this tech@kcrising.church email is stored on the Rising Tech computer. Once logged in to the Rising Tech computer, press the "Windows" key and type "Passwords and Keys" and hit enter. This will open the "Passwords and Keys" application. By default, it will show a "GnuPG Keys" page. Click the "Back" arrow in the top left to go back to the main page. Click "Login" to see logins. One of the list items is "tech@bw.lyte.dev", which you can double-click to open. A small dialog window will pop up and you can click "Copy" to copy the password for pasting later for the next step.

### Accessing bw.lyte.dev

https://bw.lyte.dev is a Bitwarden instance, which is a highly-secure way of managing shared passwords from an app (Bitwarden) or web browser. You can login with the "tech@kcrising.church" email and the password copied from the previous step OR you may request a dedicated personal account and be granted access to the same passwords. Whichever method you decide, you should now be able to open a browser tab to https://bw.lyte.dev and login using your credentials. Once logged in, you should see a variety of items under "My Vault" providing credentials to the resources they describe. For more information on using Bitwarden, see https://bitwarden.com/help/getting-started-videos/#manage-your-vault


[livestream-dashboard]: https://studio.youtube.com/channel/UCrzXqmmKCVPvGbE6yz0DJDA/livestreaming/dashboard
[livestream]: https://www.youtube.com/channel/UCrzXqmmKCVPvGbE6yz0DJDA/live
[local-lyrics-control-panel]: http://localhost:3000/control
[global-lyrics-control-panel]: http://sanctuary.kcrising.church/control
[local-lyrics]: http://localhost:3000
[global-lyrics]: http://sanctuary.kcrising.church
[audio-uploads]: https://drive.google.com/drive/u/0/folders/0ByUVRkmCWfVRRDB4TndQQ2NsUkE
[presentation-files]: https://drive.google.com/drive/u/0/folders/0B-Pj5ol_kvxMfk1PMXRPZjRvZHJtRUNaSEZCNTZVd0xiTGxzUGdZelBROVdZZGhoMTFuZEE
[obtaining-passwords]: https://gitlab.com/Rising-Church/technical-documentation/-/blob/master/readme.md#obtaining-passwords